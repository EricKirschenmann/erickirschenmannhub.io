/**
 * Eric Kirschenmann
 * February 17 2014
 * Purpose: Check the user's password for strength based on 5 given rules
 */
import javax.swing.*;
import java.awt.*;
import java.awt.event.*;


@SuppressWarnings("serial")
public class PasswordCheckGUI extends JFrame {

	//Create objects for the GUI and the password string
	Container contents;
	public JLabel [] Rules = new JLabel [6];
	public JTextField input = new JTextField("", 20);
	public JLabel inst = new JLabel("Please enter a password you would like to check.");
	public JLabel space = new JLabel("   ");
	public String password = "";
	public int check = 5;
	public JLabel strength = new JLabel("Strength: " + check);
	public boolean ruleCheck [] = new boolean [6];
		
	//set up the GUI
	PasswordCheckGUI()
	{
		//Constructor and layout
		super("pwd check");
		contents = getContentPane();
		contents.setLayout(new GridLayout(11,1));
		
		//add action listener to the text box
		input.addActionListener(new TextFieldHandler());
		
		//add elements to the GUI
		contents.add(inst);
		contents.add(input);
		contents.add(space);
		
		//Add text to the Labels
		Rules[0] = new JLabel("Password Rules:");
		Rules[1] = new JLabel("1. It must have at least 8 characters, and it must not contain any space character.");
		Rules[2] = new JLabel("2. It must contain at least one special character, which is not a letter or digit.");
		Rules[3] = new JLabel("3. It must contain at least one upper-case letter.");
		Rules[4] = new JLabel("4. It must contain at least one lower-case letter.");
		Rules[5] = new JLabel("5. It must contain at least one digit.");
		
		//add the labels to the frame
		for(int j = 0; j < 6; j++)
		{
			contents.add(Rules[j]);
		}
		
		//add the strength label
		contents.add(strength);
		
		//set the size and visibility
		setSize(500,250);
		setVisible(true);
	}
	
	//create the actionlistener
	private class TextFieldHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent ae)
		{			
			//gets the password from the user
			password = input.getText();
			//call the strength check message
			strengthCheck();
			//display password strength
			strength.setText("Password Strength: " + check);
			
			//Change the color of the password strength based on strength
			if(check >= 4)
				strength.setForeground(new Color(57,132,57));
			else if(check >= 2)
				strength.setForeground(new Color(240,173,78));
			else if(check >= 0)
				strength.setForeground(new Color(212,63,58));
			
			//change the color of the rules that are failed
			if(ruleCheck[1] == false)
				Rules[1].setForeground(new Color(212,62,58));
			if(ruleCheck[2] == false)
				Rules[2].setForeground(new Color(212,62,58));
			if(ruleCheck[3] == false)
				Rules[3].setForeground(new Color(212,62,58));
			if(ruleCheck[4] == false)
				Rules[4].setForeground(new Color(212,62,58));
			if(ruleCheck[5] == false)
				Rules[5].setForeground(new Color(212,62,58));
		}
	}
	
	//strength check method
	public void strengthCheck()
	{
		//set the strength all back to 5
		check = 5;
		
		//set all the rule text to black
		for(int j = 1; j < 6; j++)
		{
			ruleCheck[j] = true;
			Rules[j].setForeground(Color.black);
		}
		
		//check the length of the password and decrement check by 1 if it is short or has a space
		if(password.length() >= 8)
		{
			for(int j = 0; j < password.length(); j++)
			{			
				if(password.charAt(j) == ' ')
				{
					ruleCheck[1] = false;
					check--;
					break;
				}
			}
		}
		else
		{
			ruleCheck[1] = false;
			check--;
		}
		//Check if the password doesn't have a special character
		for(int x = 0; x < password.length(); x++)
		{
			if(!Character.isLetterOrDigit(password.charAt(x)) && password.charAt(x) != ' ')
				break;
			if(x == (password.length() - 1))
			{
				ruleCheck[2] = false;
				check--;
				break;
			}
		}
		//check if there is an uppercase letter
		for(int y = 0; y < password.length(); y++)
		{
			if(Character.isUpperCase(password.charAt(y)))
				break;
			if(y == (password.length() - 1))
			{
				ruleCheck[3] = false;
				check--;
				break;
			}
		}
		//check if there is a lowercase letter
		for(int u = 0; u < password.length(); u++)
		{
			if(Character.isLowerCase(password.charAt(u)))
				break;
			if(u == (password.length() - 1))
			{
				ruleCheck[4] = false;
				check--;
				break;
			}
		}
		//check if there is a digit
		for(int p = 0; p < password.length(); p++)
		{
			if(Character.isDigit(password.charAt(p)))
				break;
			if(p == (password.length() - 1))
			{
				ruleCheck[5] = false;
				check--;
				break;
			}
		}
	}
	
	//main method
	public static void main(String [] args)
	{
		PasswordCheckGUI p = new PasswordCheckGUI();
		p.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
	}
	
}

//import the arraylist and scanners
import java.util.ArrayList;
import java.util.Scanner;

public class ArrayLists {
	
	//method used to create selectionSort adjusted for arraylist
	public static ArrayList<String> selectionSort(ArrayList<String> list)
	{
		//instantiate variables for sorting
		String temp = null;
		int max = 0;
		//for loop that sorts the array
		for(int h = 0; h < list.size(); h++)
		{
			//call the method to find largest element's position
			max = indexOfLargest(list, list.size() - h);
			//sort the array list
			temp = list.get(max);
			list.set(max, list.get(list.size() - h - 1));
			list.set(list.size() - h - 1, temp);
		}
		
		//return the array list
		return list;
	}
	
	//method used to search the array list for the largest element
	public static int indexOfLargest(ArrayList<String> list, int size)
	{
		//start the index at 0
		int index = 0;
		//for loop that compares elements to find the "largest"
		for(int i = 1; i < size; i++)
		{
			if(((list.get(i)).compareTo(list.get(index))) >= 0)
				index = i;
		}
		//return the location of the largest		
		return index;
	}
	
	//method used to show the array list
	public static void showAll(ArrayList<String> list)
	{
		//enhanced for loop to display the array list
		for(String i : list)
			System.out.print(i + " ");
		System.out.println();
	}
	
	//main method that runs the program
	public static void main(String [] args)
	{
		//instantiate the array list, a scanner, and an array to store input
		ArrayList<String> list = new ArrayList<String>();
		Scanner in = new Scanner(System.in);
		String [] input;
		
		//prompt the user for commands
		System.out.println("Please enter any of these commands: insert, delete, deleteall, showlist, exit, or help.\n" + "If adding or removing items please enter them after your command.");	
		
		//never ending loop
		while(true)
		{
			//take user input and store into string then set to lowercase then split into array
			String userIn = in.nextLine();
			userIn.toLowerCase();
			input = userIn.split(" ");
			
			//if statements that check each command
			if(input[0].equals("exit"))
				System.exit(0);//exit the program
			else if(input[0].equals("insert"))
			{
				for(int x = 1; x < input.length; x++)
				{
					list.add(input[x]);//add the user's inputs
				}
				selectionSort(list);//sort the list
			}
			else if(input[0].equals("delete"))
			{
				if(!list.isEmpty())//check if the list is empty
					for(int x = 1; x < input.length; x++)
						list.remove(input[x]);//if not empty delete the user's input
				else //tell the user if the list is empty
					System.out.println("Sorry the list is empty!");
			}
			else if(input[0].equals("deleteall"))
			{
				if(!list.isEmpty()) //delete everything from the list if not empty
					list.removeAll(list); 
				else //tell the user if the list is empty
					System.out.println("Sorry the list is empty!");
			}
			else if(input[0].equals("showlist"))
			{
				if(list.isEmpty()) //if the list is empty tell the user
					System.out.println("Sorry the list is empty!");
				else
					showAll(list); //call the show all method
			}
			else if(input[0].equals("help"))
			{ //give the user help
				System.out.println("You can enter any of these commands:"
						+ "\ninsert    - add any number of items to the list"
						+ "\ndelete    - delete any number of items from the list"
						+ "\ndeletall  - remove all of the items from the list"
						+ "\nshowlist  - show the entire list"
						+ "\nexit      - end the program"
						+ "\nhelp      - this list of commands");
			}
			else //if no command found tell the user
				System.err.println("Sorry command not recognized. Please try again.");//check if the user entered a bad command
		}
		
	}
}

//import necessary packages
import java.util.Calendar;
import java.util.Random;

//board generation class
public class BoardGen{
	
	//instantiate variables, arrays,
	//and objects needed for the generation
	/**
	 * sym[0] is x,z,Z,X -- unused space holder
	 * sym[1] is 1,2,3,4
	 * sym[2] is A,B,C,D
	 * sym[3] is P,Q,R,S
	 * sym[4] is D,S,C,H 
	 **/
	public static char [][] sym = { {'!', 'X', 'X', 'X', 'X'}, {'!', '1', '2', '3', '4'},
									{'!', 'A', 'B', 'C', 'D'}, {'!', 'P', 'Q', 'R', 'S'},
									{'!', 'D', 'S', 'C', 'H'} };
	private static char baseBoards [][][] = new char [5][5][13];
	private static int bl, chn, bn = 0; //bl = boardlayout, chn = char number, bn = board number
	private Calendar cal = Calendar.getInstance(); //create calendar object
	private	Random rand = new Random(cal.getTimeInMillis()); //create random object seeded with the current time in milliseconds
	
	//method used to create random numbers from 1 to 4
	//then return that number
	private int randomGen()
	{

		int r = rand.nextInt( 4 ) + 1;
		
		return r;
	}
	
	//create an RNG to generate a board number 1 - 12
	private int randBoardGen()
	{
		int r = rand.nextInt(baseBoards[1][1].length - 1) + 1;
		
		return r;
	}
	
	//method that instantiates the baseBoards array,
	//these are the default game boards that are valid
	private void instantiate()
	{
		//First board
		baseBoards[1][1][1] = baseBoards[2][3][1] = baseBoards[3][4][1] = baseBoards[4][2][1] = '1';
		baseBoards[1][2][1] = baseBoards[2][4][1] = baseBoards[3][3][1] = baseBoards[4][1][1] = '2';
		baseBoards[1][3][1] = baseBoards[2][1][1] = baseBoards[3][2][1] = baseBoards[4][4][1] = '3';
		baseBoards[1][4][1] = baseBoards[2][2][1] = baseBoards[3][1][1] = baseBoards[4][3][1] = '4';
		
		//Second board
		baseBoards[1][1][2] = baseBoards[2][4][2] = baseBoards[3][2][2] = baseBoards[4][3][2] = '1';
		baseBoards[1][4][2] = baseBoards[2][1][2] = baseBoards[3][3][2] = baseBoards[4][2][2] = '2';
		baseBoards[1][2][2] = baseBoards[2][3][2] = baseBoards[3][1][2] = baseBoards[4][4][2] = '3';
		baseBoards[1][3][2] = baseBoards[2][2][2] = baseBoards[3][4][2] = baseBoards[4][1][2] = '4';
		
		//Third board
		baseBoards[1][1][3] = baseBoards[2][3][3] = baseBoards[3][4][3] = baseBoards[4][2][3] = '1';
		baseBoards[1][3][3] = baseBoards[2][1][3] = baseBoards[3][2][3] = baseBoards[4][4][3] = '2';
		baseBoards[1][2][3] = baseBoards[2][4][3] = baseBoards[3][3][3] = baseBoards[4][1][3] = '3';
		baseBoards[1][4][3] = baseBoards[2][2][3] = baseBoards[3][1][3] = baseBoards[4][3][3] = '4';
		
		//Fourth board
		baseBoards[1][1][4] = baseBoards[2][4][4] = baseBoards[3][2][4] = baseBoards[4][3][4] = '1';
		baseBoards[1][2][4] = baseBoards[2][3][4] = baseBoards[3][1][4] = baseBoards[4][4][4] = '2';
		baseBoards[1][4][4] = baseBoards[2][1][4] = baseBoards[3][3][4] = baseBoards[4][2][4] = '3';
		baseBoards[1][3][4] = baseBoards[2][2][4] = baseBoards[3][4][4] = baseBoards[4][1][4] = '4';
		
		//Swap 2 and 4
		//Fifth board
		baseBoards[1][1][5] = baseBoards[2][3][5] = baseBoards[3][4][5] = baseBoards[4][2][5] = '1';
		baseBoards[1][4][5] = baseBoards[2][2][5] = baseBoards[3][1][5] = baseBoards[4][3][5] = '2';		
		baseBoards[1][3][5] = baseBoards[2][1][5] = baseBoards[3][2][5] = baseBoards[4][4][5] = '3';
		baseBoards[1][2][5] = baseBoards[2][4][5] = baseBoards[3][3][5] = baseBoards[4][1][5] = '4';
		
		//Sixth board
		baseBoards[1][1][6] = baseBoards[2][4][6] = baseBoards[3][2][6] = baseBoards[4][3][6] = '1';
		baseBoards[1][3][6] = baseBoards[2][2][6] = baseBoards[3][4][6] = baseBoards[4][1][6] = '2';
		baseBoards[1][2][6] = baseBoards[2][3][6] = baseBoards[3][1][6] = baseBoards[4][4][6] = '3';
		baseBoards[1][4][6] = baseBoards[2][1][6] = baseBoards[3][3][6] = baseBoards[4][2][6] = '4';	
		
		//Seventh board
		baseBoards[1][1][7] = baseBoards[2][3][7] = baseBoards[3][4][7] = baseBoards[4][2][7] = '1';
		baseBoards[1][4][7] = baseBoards[2][2][7] = baseBoards[3][1][7] = baseBoards[4][3][7] = '2';
		baseBoards[1][2][7] = baseBoards[2][4][7] = baseBoards[3][3][7] = baseBoards[4][1][7] = '3';
		baseBoards[1][3][7] = baseBoards[2][1][7] = baseBoards[3][2][7] = baseBoards[4][4][7] = '4';
		
		//eighth board
		baseBoards[1][1][8] = baseBoards[2][4][8] = baseBoards[3][2][8] = baseBoards[4][3][8] = '1';
		baseBoards[1][3][8] = baseBoards[2][2][8] = baseBoards[3][4][8] = baseBoards[4][1][8] = '2';
		baseBoards[1][4][8] = baseBoards[2][1][8] = baseBoards[3][3][8] = baseBoards[4][2][8] = '3';
		baseBoards[1][2][8] = baseBoards[2][3][8] = baseBoards[3][1][8] = baseBoards[4][4][8] = '4';

		//Swap 3 and 4
		//ninth board
		baseBoards[1][1][9] = baseBoards[2][3][9] = baseBoards[3][4][9] = baseBoards[4][2][9] = '1';
		baseBoards[1][2][9] = baseBoards[2][4][9] = baseBoards[3][3][9] = baseBoards[4][1][9] = '2';
		baseBoards[1][4][9] = baseBoards[2][2][9] = baseBoards[3][1][9] = baseBoards[4][3][9] = '3';
		baseBoards[1][3][9] = baseBoards[2][1][9] = baseBoards[3][2][9] = baseBoards[4][4][9] = '4';

		//tenth board
		baseBoards[1][1][10] = baseBoards[2][4][10] = baseBoards[3][2][10] = baseBoards[4][3][10] = '1';
		baseBoards[1][4][10] = baseBoards[2][1][10] = baseBoards[3][3][10] = baseBoards[4][2][10] = '2';
		baseBoards[1][3][10] = baseBoards[2][2][10] = baseBoards[3][4][10] = baseBoards[4][1][10] = '3';
		baseBoards[1][2][10] = baseBoards[2][3][10] = baseBoards[3][1][10] = baseBoards[4][4][10] = '4';
		
		//eleventh board
		baseBoards[1][1][11] = baseBoards[2][3][11] = baseBoards[3][4][11] = baseBoards[4][2][11] = '1';
		baseBoards[1][3][11] = baseBoards[2][1][11] = baseBoards[3][2][11] = baseBoards[4][4][11] = '2';
		baseBoards[1][4][11] = baseBoards[2][2][11] = baseBoards[3][1][11] = baseBoards[4][3][11] = '3';
		baseBoards[1][2][11] = baseBoards[2][4][11] = baseBoards[3][3][11] = baseBoards[4][1][11] = '4';

		//twelfth board
		baseBoards[1][1][12] = baseBoards[2][4][12] = baseBoards[3][2][12] = baseBoards[4][3][12] = '1';
		baseBoards[1][2][12] = baseBoards[2][3][12] = baseBoards[3][1][12] = baseBoards[4][4][12] = '2';
		baseBoards[1][3][12] = baseBoards[2][2][12] = baseBoards[3][4][12] = baseBoards[4][1][12] = '3';
		baseBoards[1][4][12] = baseBoards[2][1][12] = baseBoards[3][3][12] = baseBoards[4][2][12] = '4';

		/**
		 *  If adding more base boards:
		 *  
		    baseBoards[1][][] = baseBoards[2][][] = baseBoards[3][][] = baseBoards[4][][] = '1';
			baseBoards[1][][] = baseBoards[2][][] = baseBoards[3][][] = baseBoards[4][][] = '2';
			baseBoards[1][][] = baseBoards[2][][] = baseBoards[3][][] = baseBoards[4][][] = '3';
			baseBoards[1][][] = baseBoards[2][][] = baseBoards[3][][] = baseBoards[4][][] = '4';
		 * 
		 */
	}
	
	//method that sets the characters in the game board
	private void setChars(char [][] board)
	{
		for(int x = 1; x < board.length; x++)
		{
			for(int y = 1; y < board[x].length; y++)
			{
				//matches each index to their corresponding char in the sym array and replaces them
				//based on the char number generated earlier
				if(board[x][y] == '1')
					board[x][y] = sym[chn][1];
				else if(board[x][y] == '2')
					board[x][y] = sym[chn][2];
				else if(board[x][y] == '3')
					board[x][y] = sym[chn][3];
				else if(board[x][y] == '4')
					board[x][y] = sym[chn][4];
				else
					board[x][y] = '!'; //if the number is not found for some reason, set it to '!'
			}
		}
	}
	
	//method that rotates, flips or mirrors the board
	private void setBoard(char [][] board)
	{
		//temporary array used for the multiple flip
		char [][] temp = new char [5][5];
		
		//Flip to the right
		if(bl == 1)
		{
			for(int x = 1; x < board.length; x++)
			{
				for(int y = 1; y < board[x].length; y++)
				{
					//checks the y value and reverses it to flip the board
					switch (y)
					{
					case 1:
						board[x][y] = baseBoards[x][4][bn];
						break;
					case 2:
						board[x][y] = baseBoards[x][3][bn];
						break;
					case 3:
						board[x][y] = baseBoards[x][2][bn];
						break;
					case 4:
						board[x][y] = baseBoards[x][1][bn];
						break;
					}
				}
			}
		}
		//Flip downwards
		else if(bl == 2)
		{
			for(int x = 1; x < board.length; x++)
			{
				for(int y = 1; y < board[x].length; y++)
				{
					//checks the x value and reverses it to flip the board
					//downward
					switch (x)
					{
					case 1:
						board[x][y] = baseBoards[4][y][bn];
						break;
					case 2:
						board[x][y] = baseBoards[3][y][bn];
						break;
					case 3:
						board[x][y] = baseBoards[2][y][bn];
						break;
					case 4:
						board[x][y] = baseBoards[1][y][bn];
						break;
					}
				}
			}
		}
		//Flip Diagonally
		//this uses the two above to flip
		//the board diagonally
		else if(bl == 3)
		{
			//First flip to the right
			for(int x = 1; x < board.length; x++)
			{
				for(int y = 1; y < board[x].length; y++)
				{
					switch(y)
					{
					case 1:
						temp[x][y] = baseBoards[x][4][bn];
						break;
					case 2:
						temp[x][y] = baseBoards[x][3][bn];
						break;
					case 3:
						temp[x][y] = baseBoards[x][2][bn];
						break;
					case 4:
						temp[x][y] = baseBoards[x][1][bn];
						break;
					}
				}
			}
			
			//Second flip downwards
			
			for(int x = 1; x < board.length; x++)
			{
				for(int y = 1; y < board[x].length; y++)
				{
					switch(x)
					{
					case 1:
						board[x][y] = temp[4][y];
						break;
					case 2:
						board[x][y] = temp[3][y];
						break;
					case 3:
						board[x][y] = temp[2][y];
						break;
					case 4:
						board[x][y] = temp[1][y];
						break;
					}
				}
			}
		}
		//Normal Board when bn = 4
		else
		{
			for(int x = 1; x < board.length; x++)
			{
				for(int y = 1; y < board[x].length; y++)
				{
					//set the board to default layout
					board[x][y] = baseBoards[x][y][bn];
				}
			}
		}
		
		
	}
	
	public void createBoard(char [][] b)
	{
		//First Choose a board
		bn = randBoardGen();
		//Second choose board layout
		bl = randomGen();
		//Third Choose characters
		chn = randomGen();
		
		//sets the board up
		setBoard(b);
		//sets the characters
		setChars(b);
				
	}
	
	//method used to delete 10 items from the board
	public void remove(char [][] b)
	{
		//variable to track how many removals
		//and how many have been removed from
		//each row
		int count = 0;
		int total [] = new int [5];
		
		//loop that waits for 10 removals
		while(count != 10)
		{
			//get random values
			int r = randomGen();
			int c = randomGen();
			
			//makes sure only 3 items are removed from each row to avoid empty rows
			if(total[r] < 3)
			{
				//if the space is not already blank remove the item
				if(b[r][c] != '-')
				{
					b[r][c] = '-';
					count++;
					total[r]++;
				}
			}
		}
	}
	
	//returns the chars being used in the current game
	public void getSym(char [] b)
	{
		for(int x = 1; x < b.length; x++)
		{
			b[x] = sym[chn][x];
		}
	}
	
	//method that returns the board number being used
	public int getBoardNum()
	{
		return bn;
	}
	//method that returns the board rotation being used
	public int getRotNum()
	{
		return bl;
	}
	//method that returns the char set being used
	public int getCharNum()
	{
		return chn;
	}
	//constructor
	public BoardGen()
	{
		//isntantiate the baseBoards on creation
		instantiate();
	}
	
}

/**
 * Author: Eric Kirschenmann
 * Date: April 27, 2014
 * Purpose: Create a basic sym4 game with a GUI
 */

//import necessary packages
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.FlowLayout;
import java.awt.Font;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;

//create a GUI that gives the user basic instructions
@SuppressWarnings("serial")
class Options extends JFrame{
	
	//create necessary parts for the GUI
	Container contents;
	JTextArea inst = new JTextArea("This game is an easier"
			+ " version of sudoku. \nTo play fill in the missing"
			+ " items to complete the board.\nThere must be"
			+ " one of each of the\nsymbols in each: row, column"
			+ " square and diagonal.");
	JButton okay = new JButton("Okay");
	
	//constructor that build the GUI
	public Options()
	{
		//the constructor for JFrame
		super("Instructions");
		//get the content pane and set up layout
		contents = getContentPane();
		contents.setLayout(new FlowLayout());
		
		//make the text area uneditable and then change the font
		//then add to the GUI
		inst.setEditable(false);
		inst.setFont(new Font("Arial", Font.PLAIN, 20));
		contents.add(inst);
		
		//add an actionlistener to the button then add the button to the GUI
		okay.addActionListener(new ButtonHandler());
		contents.add(okay);
		
		//set default values for the GUI
		//such as visibility and size
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setVisible(true);
		setSize(500,300);
	}
	
	//implement action listener abstract class
	private class ButtonHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent ae)
		{
			//if the user presses the okay button, close the jframe
			if(ae.getSource() == okay)
			{
				dispose();
			}
		}
	}

}

//create a GUI that will provide game stats then prompt
//the user if they would like to play again
@SuppressWarnings("serial")
class newGame extends JFrame {

	//instantiate necessary GUI components
	Container contents;
	//This text area contains information on current scores and averages
	JTextArea info = new JTextArea("Congrats. You won! Here are your stats: " +"\nGames Played:  " + Game.gamesPlayed
										+ "\nTotal Score: " + Game.totalScore + "\nCurrent Score: " + Game.cgScore
										+ "\nAverage Score: " + (((double)(Game.totalScore)/(double)(Game.gamesPlayed))) + " points"
										+ "\nWould you like to play another game?");
	JButton newg = new JButton("Yes"); //newg = new game
	JButton nog = new JButton("No"); //nog = no game
	
	//constructor
	newGame()
	{
		//constructor for JFrame
		super("Game Info");
		
		//get the content pane then set the layout
		contents = getContentPane();
		contents.setLayout(new FlowLayout());
		
		//add button handlers and set the text area to uneditable
		newg.addActionListener(new ButtonHandler());
		nog.addActionListener(new ButtonHandler());
		info.setEditable(false);
		
		//add the components
		contents.add(info);
		contents.add(newg);
		contents.add(nog);
		
		//set the size and visibility
		setSize(250,250);
		setVisible(true);
	}
	
	//implement the actionlistener abstract class to create buttonhandler
	private class ButtonHandler implements ActionListener
	{
		public void actionPerformed(ActionEvent ae)
		{
			//if the user says yes to a new game
			if(ae.getSource() == newg)
			{
				Game.gamesPlayed++; //increment the total games played
				Game.startGame(); //call the start game method
				Game gb = new Game(Game.b); //create a new gameboard
				gb.setVisible(true); //make it visible
				dispose(); //dispose of the newGame GUI window
			}
			//if they don't want to play a new game exit the program
			else if(ae.getSource() == nog)
			{
				dispose();
			}
		}
	}
}
	

//main class that runs the program and creates
//the game board GUI
@SuppressWarnings("serial")
public class Game extends JFrame {
	
	//variables used throughout the program
	public static char [][] b = new char [5][5];
	public static char [][] ansCheck = new char [5][5];
	public static char used [] = new char [5];
	public static boolean [] opSel = new boolean [5];
	private static int n = 0;
	public static int totalScore = 0;
	public static int cgScore = 0;
	public static int gamesPlayed = 1;
	public static int moves = 0;
	
	//instantiate necessary GUI components
	Container contents;
	JButton [][] game = new JButton [5][5];
	JPanel board = new JPanel();
	JButton submit = new JButton("Submit");
	JButton quit = new JButton("Quit");
	JButton score = new JButton("Score");
	JPanel options = new JPanel();
	JLabel current = new JLabel("Game: ");
	JLabel [] blank = new JLabel [2];
	JButton [] choose = new JButton[5];
	JPanel select = new JPanel();
	
	//constructor
	public Game(char [][] b)
	{
		//JFrame constructor which puts the
		//current game number in the title
		super("Sym4 Game " + gamesPlayed);
		//get content pane and set layout
		contents = getContentPane();
		contents.setLayout(new BorderLayout(55,50));
		
		//create individual gridlayouts for different
		//sections of the GUI
		GridLayout gl = new GridLayout(4,4);
		GridLayout butt = new GridLayout(1,3);
		GridLayout sel = new GridLayout(1,4);
		//add these layouts to the JPanels
		board.setLayout(gl);
		options.setLayout(butt);
		select.setLayout(sel);
		
		//instantiate various buttons
		//and add action listeners
		//and change fonts
		for(int x = 1; x < game.length; x++)
		{
			choose[x] = new JButton("" + used[x]);
			choose[x].addActionListener(new SelectionButton());
			for(int y = 1; y < game[x].length; y++)
			{
				game[x][y] = new JButton("" + b[x][y]);
				game[x][y].setFont(new Font("Arial", Font.BOLD, 18));
				game[x][y].setBackground(Color.WHITE);
				game[x][y].addActionListener(new GameBoard());
				
				if(!(game[x][y].getText().equals("-")))
					game[x][y].setEnabled(false);
				
			}
			
		}
		
		//add the buttons to the JPanels
		for(int x = 1; x < game.length; x++)
		{
			select.add(choose[x]);
			
			for(int y = 1; y < game[x].length; y++)
				board.add(game[x][y]);
		}
		
		//blank jlabels that are used for side spacing
		blank[0] = new JLabel(" ");
		blank[1] = new JLabel(" ");
		
		//add action listeners to the different option buttons
		submit.addActionListener(new OptionButton());
		quit.addActionListener(new OptionButton());
		score.addActionListener(new OptionButton());
		
		//add the option buttons to their jpanel
		options.add(submit);
		options.add(quit);
		options.add(score);
		
		//add everything to the GUI in the different
		//borderlayout locations
		contents.add(blank[0], BorderLayout.EAST);
		contents.add(blank[1], BorderLayout.WEST);
		contents.add(select, BorderLayout.NORTH);
		contents.add(board,BorderLayout.CENTER);	
		contents.add(options, BorderLayout.SOUTH);
		
		//set teh size and make it un-resizable
		setSize(500,500);
		setResizable(false);
		
		
	}
	//implement actionlistener abstract class for the gameboard
	private class GameBoard implements ActionListener
	{
		public void actionPerformed(ActionEvent ae)
		{
			for(int x = 1; x < game.length; x++)
			{
				for(int y = 1; y < game[x].length; y++)
				{
					//checks for button pushes
					if(ae.getSource() == game[x][y])
					{
						
						for(int z = 1; z < opSel.length; z++)
						{
							//checks which char they're adding to the board
							//and makes the move
							if(opSel[z])
							{
								//increment the move
								//add the char to the board
								//and the array
								moves++;
								String get;
								game[x][y].setText("" + used[z]);
								get = game[x][y].getText();
								b[x][y] = get.charAt(0);
							}
						}
					}
				}
			}
		}
	}
	
	//implement the actionlistener abstract class for the selection buttons
	//ie A,B,C,D or 1,2,3,4
	private class SelectionButton implements ActionListener
	{
		public void actionPerformed(ActionEvent ae)
		{
			for(int x = 1; x < choose.length; x++)
			{
				//if the button is selected
				if(ae.getSource() == choose[x])
				{
					//set the corresponding boolean
					//to true and change the font color to Red
					opSel[x] = true;
					choose[x].setForeground(Color.RED);
					
					//go through and set all the other
					//buttons to black and their booleans to false
					for(int y = 1; y < opSel.length; y++)
					{
						if(y != x)
						{
							opSel[y] = false;
							choose[y].setForeground(Color.BLACK);
						}

							
					}
				}
			}
		}
	}
	
	//implement actionlistener abstract class for the three option buttons
	private class OptionButton implements ActionListener
	{
		public void actionPerformed(ActionEvent ae)
		{
			//if the user selects quit kill the program
			if(ae.getSource() == quit)
			{
				dispose();
			}
			//if the user selects the submit button
			else if(ae.getSource() == submit) 
			{
				//check if the game board is solved by calling the game check method
				if(!gameCheck())
				{
					//alert the user that the game is not finished
					JOptionPane.showMessageDialog(null, "Sorry you did not complete the game. Keep trying :)" , "Sorry", JOptionPane.ERROR_MESSAGE);
					
				}
				else
				{
					//if the game is done
					//generate their score, kill the window and then
					//create a newGame GUI
					scoreGen();
					dispose();
					newGame ng = new newGame();
					ng.setAlwaysOnTop(true);
					
				}
				
			}
			//if the user clicks the score button
			//output their current score
			else if(ae.getSource() == score)
			{
				JOptionPane.showMessageDialog(null, "Games Played: " + gamesPlayed + "\nTotal Score: " + totalScore + "\nCurrent Move: " + moves, "Score", JOptionPane.PLAIN_MESSAGE);
				
			}
		}
	}
	
	//method used to start the game up
	public static void startGame()
	{
		//set score and moves to 0
		cgScore = 0;
		moves = 0;
		//create new boardgen object
		BoardGen bg = new BoardGen();
		//call the create board and get symbol methods
		//passing arrays as parameters
		bg.createBoard(b);
		bg.getSym(used);
		//save the current board into the answer check array
		setArray(b);
		//call the remove method
		bg.remove(b);
		
		//information regarding boards and other things
		showBoards(bg);
		diag(bg);
	}
	
	//method to generate the score
	public static void scoreGen()
	{
		//if they used 10 moves give them 10 points
		if(moves == 10)
			cgScore = 10;
		else
			cgScore = 20 - moves; //otherwise subtract their points from 20

		//calculate the total score
		totalScore = totalScore + cgScore;

	}
	
	//method to check if the game is done
	public static boolean gameCheck()
	{

		for(int x = 1; x < b.length; x++)
		{
			for(int y = 1; y < b[x].length; y++)
			{
				//if the board doesn't match
				//the answer key return false
				if(!(b[x][y] == ansCheck[x][y]))
				{
					return false;
				}
			}
		}
		//if it makes it through the game is complete
		//return true
		return true;
	}
	//method that creates the answer check array
	public static void setArray(char b[][])
	{
		for(int x = 1; x < b.length; x++)
		{
			for(int y = 1; y < b[x].length; y++)
				ansCheck[x][y] = b[x][y];
		}
	}
	
	//method that shows the board
	//before removing items then after
	//only used for debugging purposes
	public static void showBoards(BoardGen bg)
	{
		n = bg.getBoardNum();
		
		System.out.println("Board " + n + ": ");
		for(int x = 1; x < b.length; x++)
		{
			for(int y = 1; y < b[x].length; y++)
			{
				System.out.print(b[x][y] + " ");
			}
			System.out.println();
		}
		
		System.out.println("\nBoard " + n + ": ");
		for(int x = 1; x < b.length; x++)
		{
			for(int y = 1; y < b[x].length; y++)
			{
				System.out.print(ansCheck[x][y] + " ");
			}
			System.out.println();
		}
		
		System.out.println("\nSyms used:");
		for(int x = 1; x < used.length; x++)
		{
			System.out.print(used[x] + " ");
		}
		System.out.println();
	}
	
	//method used to display which char set, board, and rotation
	//are being used. Only for debugging purposes
	public static void diag(BoardGen bg)
	{
		int n = bg.getCharNum();
		System.out.println("Char set: " + n);
		int m = bg.getBoardNum();
		System.out.println("Board: " + m);
		int o = bg.getRotNum();
		System.out.println("Rotation: " + o);

	}
	
	//main method that starts the game
	public static void main(String [] args)
	{
		//instantiate boardgen object
		BoardGen bg = new BoardGen();
		//call the methods to create
		//the board and get symbols
		bg.createBoard(b);
		bg.getSym(used);
		//create the answercheck array
		setArray(b);
		//remove items from the board
		bg.remove(b);
		
		//show the board and debug info
		showBoards(bg);
		diag(bg);
		
		//create new option GUI and set it to always on top
		Options op = new Options();
		op.setAlwaysOnTop(true);

		//create the gameboard GUI
		Game g = new Game(b);		
		g.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		//wait for the option GUI to be closed before
		//making the game visible
		while(op.isVisible())
		{
			g.setVisible(false);
		}
		g.setVisible(true);

	}	
	
}

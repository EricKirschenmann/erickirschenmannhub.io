/**
 * Author: Eric Kirschenmann
 * Purpose: Use a linked list to store or remove or show items based on user commands
 */

import java.util.Scanner;

/**
 * Class to set up the nodes in the linked list
 *
 */
class Nodes{
	//instantiate the list
	public String list;
	public Nodes next;
	
	//construct the nodes
	public Nodes(String newData)
	{
		list = newData;
		next = null;
	}
	
	//add nodes
	public Nodes(String newData, Nodes newNode)
	{
		setData(newData);
		next = newNode;
	}
	
	//add data to the nodes
	public void setData(String newData)
	{
		list = newData;
	}
}

/**
 * Class that utilizes the nodes to create linked lists
 * and use them base on user commands
 *
 */
public class LinkedLists {
	//instantiate an empty linked list
	private Nodes head = null;
	private int numberOfItems;
	
	//construct the initial list
	public LinkedLists()
	{
		numberOfItems = 0;
		head = null;
	}
	
	//return the size of the list
	public int getSize()
	{
		return numberOfItems;
	}	
	
	//get the string at the index provided
	public String getData(int i)
	{
		Nodes find = search(i);
		String get = find.list;
		return get;
	}
	
	//check if the list is empty
	public boolean isEmpty()
	{
		if(numberOfItems == 0)
			return true;
		else
			return false;
	}
	
	//search the list for an item
	public Nodes search(int a)
	{
		Nodes find = head;
		for(int i = 0; i < a; i++)
			find = find.next;
		return find;
	}
	
	//delete everything in the list
	public void deleteAll()
	{
		head = null;
		numberOfItems = 0;
	}
	
	//search the list for the item to be deleted and then remove it
	public void delete(String in)
	{
		if(in.equals(this.getData(0)))
		{
			head = head.next;
			numberOfItems--;
		}
		else if(in.equals(this.getData(numberOfItems - 1)))
		{
			Nodes prev = search(numberOfItems - 2);
			prev.next = null;
			numberOfItems--;
		}
		else
		{
			for(int j = 0; j < numberOfItems; j++)
			{
				if(in.equals(this.getData(j)))
				{
					Nodes prev = search(j - 1);
					Nodes curr = prev.next;
					prev.next = curr.next;
					numberOfItems--;
					break;
				}
			}
		}
		
	}
	
	//insert a string into the list and sort it dynamically
	public void insert(String in)
	{
		if(this.isEmpty())
		{
			Nodes dataList = new Nodes(in);
			head = dataList;
		}
		else if(in.compareTo(this.getData(numberOfItems - 1)) >= 0)
		{
			Nodes prev = search(numberOfItems - 1);
			Nodes newItem = new Nodes(in);
			prev.next = newItem;
		}
		else if(in.compareTo(this.getData(0)) <= 0)
		{
			Nodes newItem = new Nodes(in, head);
			head = newItem;
		}
		else
		{
			for(int j = 1; j < numberOfItems; j++)
			{
				if(in.compareTo(this.getData(j)) <= 0)
				{
					Nodes prev = search(j - 1);
					Nodes newItem = new Nodes(in, prev.next);
					prev.next  = newItem;
					break;
				}
			}
		}
		
		numberOfItems++;
	}
	
	//main method that uses the linked list
	public static void main(String [] args)
	{
		//set up scanner and list
		Scanner input = new Scanner(System.in);
		LinkedLists list = new LinkedLists();
		
		//prompt user with commands
		System.out.println("Please enter any of these commands: insert, delete, deleteall, showlist, exit, or help.\n" + "If adding or removing items please enter them after your command.");	
			
		//infinite loop
		while(true)
		{
			//take user input, set it to lowercase then split into an array
			String userIn = input.nextLine();
			userIn.toLowerCase();
			String [] data = userIn.split(" ");
			
			//check if the user wants to exit the program
			if(data[0].equals("exit"))
				System.exit(0);
			else if(data[0].equals("insert")) //check if the user wants to insert items
				for(int i = 1; i < data.length; i++)
					list.insert(data[i]);
			else if(data[0].equals("delete"))//check if the user wants to delete items
			{
				if(!list.isEmpty())
					for(int i = 1; i < data.length; i++)
						list.delete(data[i]);
				else
					System.out.println("Sorry the list is empty!");
			}
			else if(data[0].equals("deleteall"))//check if the user wants to delete all the items
			{
				if(!list.isEmpty())
					list.deleteAll();
				else
					System.out.println("Sorry the list is empty!");
			}
			else if(data[0].equals("showlist"))//check if the user wants to show the list
			{
				if(!list.isEmpty())
				{
					System.out.print("The list is: ");
					for(int i = 0; i < list.getSize(); i++)
						System.out.print(list.getData(i) + " ");
					System.out.println();
				}
				else
					System.out.println("Sorry the list is empty!");
			}
			else if(data[0].equals("help"))//check if the user wants help
			{
				System.out.println("You can enter any of these commands:"
						+ "\ninsert    - add any number of items to the list"
						+ "\ndelete    - delete any number of items from the list"
						+ "\ndeletall  - remove all of the items from the list"
						+ "\nshowlist  - show the entire list"
						+ "\nexit      - end the program"
						+ "\nhelp      - this list of commands");
			}
			else
				System.err.println("Sorry command not recognized. Please try again.");//check if the user entered a bad command
		}
	}
}
